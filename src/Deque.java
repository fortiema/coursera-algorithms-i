import java.util.Iterator;

/**
 * =======================================
 * Coursera - Algorithms I
 * Programming Assignment 2 - Queues
 * User: Mathieu Fortier (fortiema@gmail.com)
 * Date: March 2013
 * -----
 * Class: Deque
 * This class offers a generalized implementation of a double-ended queue
 * =======================================
 */
public class Deque<Item> implements Iterable<Item> {

    private int N;

    private Node m_FirstNode = null;
    private Node m_LastNode = null;

    private class Node {
        Item item;
        Node next;
        Node previous;
    }


    // --------------- CONSTRUCTORS ----------------


    public Deque() {
        N = 0;
        m_FirstNode = null;
        m_LastNode = null;
        assert checkIntegrity();
    }


    // ---------------  PUBLIC API  ----------------


    public boolean isEmpty() {
        return (m_FirstNode == null);
    }

    public int size() {
        return N;
    }

    public void addFirst(Item item) {
        if (item == null) throw new NullPointerException();

        Node NewFirst = new Node();
        NewFirst.item = item;
        NewFirst.next = m_FirstNode;
        m_FirstNode.previous = NewFirst;
        m_FirstNode = NewFirst;
        m_FirstNode.previous = null;

        N++;
        //assert checkIntegrity();
    }

    public void addLast(Item item) {
        if (item == null) throw new NullPointerException();

        Node last = new Node();
        last.item = item;
        last.next = null;
        m_LastNode.next = last;
        m_LastNode = last;

        N++;
        //assert checkIntegrity();
    }

    public Item removeFirst() {
        if (this.isEmpty()) throw new java.util.NoSuchElementException();

        Node OldFirst = m_FirstNode;
        m_FirstNode = OldFirst.next;
        m_FirstNode.previous = null;

        N--;
        assert checkIntegrity();
        return OldFirst.item;
    }

    public Item removeLast() {
        if (this.isEmpty()) throw new java.util.NoSuchElementException();

        Node OldLast = m_LastNode;
        m_LastNode = OldLast.previous;
        m_LastNode.next = null;

        N--;
        assert checkIntegrity();
        return OldLast.item;
    }

    public Iterator<Item> iterator() { return new DequeIterator(); }


    // ---------------- INTERNAL -------------------


    /**
     * Do basic consistency checks
     * @return True if Deque integrity is good
     */
    private boolean checkIntegrity() {
        if (N == 0) {
            if (m_FirstNode != null) return false;
            if (m_LastNode != null) return false;
        }
        else if (N == 1) {
            if (m_FirstNode == null) return false;
            if (m_LastNode != m_FirstNode) return false;
            if (m_FirstNode.next != null) return false;
        }
        else {
            if (m_FirstNode == m_LastNode) return false;
        }

        int NodeNb = 0;
        for (Node x = m_FirstNode; x != null; x = x.next) {
            NodeNb++;
        }
        if (NodeNb != N) return false;

        return true;
    }

    private class DequeIterator implements Iterator<Item> {
        private Node m_CurrNode = m_FirstNode;

        public Item next() {
            if (!hasNext()) throw new java.util.NoSuchElementException();
            Item item = m_CurrNode.item;
            m_CurrNode = m_CurrNode.next;
            return item;
        }
        public boolean hasNext() { return (m_CurrNode.next != null); }

        public void remove() {throw new UnsupportedOperationException();}

    }

}
