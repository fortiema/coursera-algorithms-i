import java.util.Iterator;

/**
 * =======================================
 * Coursera - Algorithms I
 * Programming Assignment 2 - Queues
 * User: Mathieu Fortier (fortiema@gmail.com)
 * Date: March 2013
 * -----
 * Class: RandomizedQueue
 * This class offers a generalized implementation of a randomized queue
 * =======================================
 */
public class RandomizedQueue<Item> implements Iterable<Item> {

    private int N;

    private Node m_FirstNode = null;
    private Node m_LastNode = null;

    private class Node {
        Item item;
        Node next;
        Node previous;
    }


    // --------------- CONSTRUCTORS ----------------


    public RandomizedQueue() {
        N = 0;
        m_FirstNode = null;
        m_LastNode = null;
        assert checkIntegrity();
    }


    // ---------------  PUBLIC API  ----------------


    public boolean isEmpty() {
        return (m_FirstNode == null);
    }

    public int size() {
        return N;
    }


    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }


    // ---------------- INTERNAL -------------------


    /**
     * Do basic consistency checks
     * @return True if Deque integrity is good
     */
    private boolean checkIntegrity() {
        if (N == 0) {
            if (m_FirstNode != null) return false;
            if (m_LastNode != null) return false;
        }
        else if (N == 1) {
            if (m_FirstNode == null) return false;
            if (m_LastNode != m_FirstNode) return false;
            if (m_FirstNode.next != null) return false;
        }
        else {
            if (m_FirstNode == m_LastNode) return false;
        }

        int NodeNb = 0;
        for (Node x = m_FirstNode; x != null; x = x.next) {
            NodeNb++;
        }
        if (NodeNb != N) return false;

        return true;
    }

    private class RandomizedQueueIterator implements Iterator<Item> {
        private Node m_CurrNode = m_FirstNode;

        public Item next() {
            if (!hasNext()) throw new java.util.NoSuchElementException();
            Item item = m_CurrNode.item;
            m_CurrNode = m_CurrNode.next;
            return item;
        }
        public boolean hasNext() { return (m_CurrNode.next != null); }

        public void remove() {throw new UnsupportedOperationException();}

    }


}
