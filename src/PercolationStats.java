/**
 * =======================================
 * Coursera - Algorithms I
 * Programming Assignment 1 - Percolation
 * User: Mathieu Fortier  (fortiema@gmail.com)
 * Date: February 2013
 * -----
 * Class: PercolationStats
 * =======================================
 */
public class PercolationStats {

    private int T;
    private double[] mStats;

    /**
     * Perform T independent computational experiments on an N-by-N grid
     */
    public PercolationStats(int N, int T) {
        if (0 >= N || 0 >= T) throw new IllegalArgumentException();
        else {

            this.T = T;
            mStats = new double[T];

            for (int z = 0; z < T; z++) {

                Percolation percolationModel = new Percolation(N);
                int count = 0;

                while (!percolationModel.percolates()) {
                    int i = StdRandom.uniform(N) + 1;
                    int j = StdRandom.uniform(N) + 1;
                    if (!percolationModel.isOpen(i, j)) {
                        percolationModel.open(i, j);
                        count++;
                    }
                }

                mStats[z] = (count / ((double) N * (double) N));

            }
        }
    }

    /**
     * Sample mean of percolation threshold
     */
    public double mean() {
        return StdStats.mean(mStats);
    }

    /**
     * Sample standard deviation of percolation threshold
     */
    public double stddev() {
        return StdStats.stddev(mStats);
    }

    /**
     * Returns lower bound of the 95% confidence interval
     */
    public double confidenceLo() {
        return (this.mean() - ((1.96 * this.stddev()) / Math.sqrt(T)));
    }

    /**
     * Returns upper bound of the 95% confidence interval
     */
    public double confidenceHi() {
        return (this.mean() + ((1.96 * this.stddev()) / Math.sqrt(T)));
    }


    public static void main(String[] args) {
        // Read input
        StdOut.println("Coursera - Algorithms I");
        StdOut.println("Assignment I: Monte Carlo Simulation & System Percolation "
                + "Analysis");
        StdOut.println("By Matt Fortier, Feb. 2013");
        StdOut.println("==========================================================");

        int n = Integer.parseInt(args[0]);
        int t = Integer.parseInt(args[1]);

        // Run simulation
        Stopwatch stopwatch = new Stopwatch();
        PercolationStats percStats = new PercolationStats(n, t);
        StdOut.print("N: " + n + " | ");
        StdOut.print("T: " + t + " | ");
        StdOut.println("Elapsed Time: " + stopwatch.elapsedTime() + "s");
        StdOut.println("-----");

        // Output Results
        StdOut.print("mean                 = ");
        StdOut.println(percStats.mean());

        StdOut.print("stddev               = ");
        StdOut.println(percStats.stddev());

        StdOut.print("95% conf. interval   = ");
        StdOut.print(percStats.confidenceLo());
        StdOut.print(", ");
        StdOut.println(percStats.confidenceHi());
    }
}
