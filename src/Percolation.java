/**
 * =======================================
 * Coursera - Algorithms I
 * Programming Assignment 1 - Percolation
 * User: Mathieu Fortier (fortiema@gmail.com)
 * Date: February 2013
 * -----
 * Class: Percolation
 * This class offers an API to create and manipulate a percolation data set
 * =======================================
 */
public class Percolation {

    private WeightedQuickUnionUF quickUnionUF;
    private boolean[][] grid;

    private final int N;
    private final int VIRTUAL_TOP;
    private final int VIRTUAL_BOTTOM;


    /**
     * Creates N-by-N grid, with all sites blocked
     */
    public Percolation(int N) {
        this.N = N;
        this.grid = new boolean[N][N];

        VIRTUAL_TOP = 0;
        VIRTUAL_BOTTOM = N * N + 1;

        quickUnionUF = new WeightedQuickUnionUF((this.N * this.N) + 2);
    }

    /**
     * Opens site (i,j) if it is not already
     */
    public void open(int i, int j) {
        if (1 > i || i > N || 1 > j || j > N)
            throw new java.lang.IndexOutOfBoundsException();

        if (this.isOpen(i, j)) return;

        grid[i-1][j-1] = true;

        int curSite = ((i-1) * N) + (j);

        // If newly opened site is on top or bottom row, connect with the virtual
        // site
        if (i == 1)
            quickUnionUF.union(curSite, VIRTUAL_TOP);

        if (i == N)
            quickUnionUF.union(curSite, VIRTUAL_BOTTOM);

        // Connect newly opened site to adjacent open sites
        if ((j > 1) && this.isOpen(i, j - 1)) {
            quickUnionUF.union(curSite, curSite - 1);
        }
        if ((j < N) && this.isOpen(i, j + 1)) {
            quickUnionUF.union(curSite, curSite + 1);
        }
        if ((i > 1) && this.isOpen(i - 1, j)) {
            quickUnionUF.union(curSite, curSite - N);
        }
        if ((i < N) && this.isOpen(i + 1, j)) {
            quickUnionUF.union(curSite, curSite + N);
        }

    }

    /**
     * Is site (i,j) open?
     */
    public boolean isOpen(int i, int j) {
        if (1 > i || i > N || 1 > j || j > N)
            throw new java.lang.IndexOutOfBoundsException();
        else {
            return grid[i-1][j-1];
        }
    }

    /**
     * Is site (i,j) full?
     */
    public boolean isFull(int i, int j) {
        if (1 > i || i > N || 1 > j || j > N)
            throw new java.lang.IndexOutOfBoundsException();
        else {
            return this.isOpen(i, j)
                    && (quickUnionUF.connected(((i-1) * N) + (j), VIRTUAL_TOP));
        }
    }

    /**
     * Does the system percolate?
     */
    public boolean percolates() {
        return quickUnionUF.connected(VIRTUAL_BOTTOM, VIRTUAL_TOP);
    }

}
