import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: Mathieu
 * Date: 09/03/13
 * Time: 11:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class PercolationTest {
    @Test
    public void testOpen() throws Exception {

    }

    @Test
    public void testIsOpen() throws Exception {
        Percolation percModel = new Percolation(10);

        percModel.open(1,1);
        assertTrue(percModel.isOpen(1,1));

        percModel.open(2,1);
        assertTrue(percModel.isOpen(2,1));

        percModel.open(10,10);
        assertTrue(percModel.isOpen(10,10));

        assertFalse(percModel.isOpen(9,9));
        assertFalse(percModel.isOpen(1,2));
    }

    @Test
    public void testIsFull() throws Exception {
        Percolation percModel = new Percolation(10);

        percModel.open(1,1);
        assertTrue(percModel.isFull(1,1));

        percModel.open(2,1);
        assertTrue(percModel.isFull(2,1));

        percModel.open(3,1);
        assertTrue(percModel.isFull(3,1));

        percModel.open(10,10);
        assertFalse(percModel.isFull(10,10));
    }

    @Test
    public void testPercolates() throws Exception {

    }
}
